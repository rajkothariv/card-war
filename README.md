**TASK-AIM:** To develop card game (war) using any HTML5 game engine

>Detail Description available at: https://docs.google.com/document/d/12Z_djBjPiCEj6O5e6MQJmHZ8AX_Rt8MtDXY35MwSJc8/edit

Tools and Technologies
------------------------------
|  |  |
|--|--|
| Game Engine | [Unity 3D](https://unity3d.com/get-unity/download) |
| Live URL | [http://talecup.com/raj](http://talecup.com/raj) |

## Game Rules:

**Goal:** The goal is to be the first player to win all 52 cards  

**Deal:** The deck is divided evenly, with each player receiving 26 cards, dealt one at a time, face down. Anyone may deal first. Each player places his stack of cards face down, in front of him.  

**Play:** Each player turns up a card at the same time and the player with the higher card takes both cards and puts them, face down, on the bottom of his stack.   
If the turned-up cards are the same rank, any random player from the two wins.  

---
